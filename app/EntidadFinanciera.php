<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntidadFinanciera extends Model
{
    protected $table ='entidad_financiera';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nombre', 'entidad','cuit','estado',
    ];

}
