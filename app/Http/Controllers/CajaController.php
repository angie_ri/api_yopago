<?php

namespace App\Http\Controllers;

use App\Caja;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\User;

class CajaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');

    }

    /**
     * crea una caja de ahoros
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function newCaja(Request $request){

        $user= User::find($request->id_user);

        if($user){

            try {
                DB::beginTransaction();
                $caja_ahorro = Caja::create([
                    'id_cuenta' => $user->cuenta()->id,
                    'id_user' => Auth::id(),
                    'estado' => 1,
                ]);
                DB::commit();
                return response()->json($caja_ahorro);

            } catch (\Throwable $e) {
                DB::rollback();
                return response()->json(['message' => 'Error al abrir caja!', 'error' =>$e], 404);
            }
        }else{
            return response()->json(['message' => 'Error al abrir caja!'], 404);
        }
    }

    /**
     * retorna caja de id_user
     * @param $id_user
     * @return \Illuminate\Http\JsonResponse
     */
    public  function getCaja($id_user)
    {
        $caja_ahorro = Caja::where('id_user', $id_user)
            ->where('estado',1)->first();

        if($caja_ahorro){
            return response()->json($caja_ahorro,200);
        }else{
            return response()->json(['message'=>'No se encontro caja'],404);
        }
    }

    /**
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * saldo por id de caja
     */
    public  function  getSaldo($id)
    {
        $saldo = Caja::where('id',$id)
            ->where('estado',1)->first(['saldo']);

        if($saldo){
            return response()->json($saldo,200);
        }else{
            return response()->json(['message'=>'No se encontro saldo'],404);
        }
    }

    /**
     * @param Request $id
     * @return \Illuminate\Http\JsonResponse
     * retorna deuda por id caja
     */
    public  function  getDeuda($id)
    {
        $deuda = Caja::where('id',$id)
            ->where('estado',1)->first(['deuda']);

        if($deuda){
            return response()->json($deuda,200);
        }else{
            return response()->json(['message'=>'No se encontro deuda'],404);
        }
    }

    /**
     * cambia saldo de caja
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function setSaldo(Request $request)
    {
        $caja_ahorro = Caja::find($request->id);
        $caja_ahorro->saldo = $request->saldo;
        $caja_ahorro->save();

        if($caja_ahorro)
        {
            return response()->json($caja_ahorro,200);
        }else{
            return response()->json(['message'=>'No se encontro caja'],404);
        }

    }

    /**
     * Cambia deuda de caja
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function  setDeuda(Request $request)
    {
        $caja_ahorro = Caja::find($request->id);
        $caja_ahorro->deuda = $request->deuda;
        $caja_ahorro->save();

        if ($caja_ahorro) {
            return response()->json($caja_ahorro, 200);
        } else {
            return response()->json(['message' => 'No se encontro caja'], 404);
        }
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *cierra caja,el estado es 2
     */
    public function  cerrarCaja(Request $request)
    {
        $caja = Caja::find($request->id);
        $caja->estado = 2;
        $caja->save();
        if($caja){
            return response()->json(['message'=>'caja cerrada!'],200);
        }else{
            return response()->json(['message'=>'No se encontro caja'],404);
        }

    }


}
