<?php

namespace App\Http\Controllers;

use App\EntidadFinanciera;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class EntidadFinancieraController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');

    }

    /**
     *Registra entidad bancaria
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function registrasEntidadFinanciera(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'entidad' => 'required',
            'cuit' => 'required|max:12',

        ]);

        try{
            DB::beginTransaction();

                $entidad = EntidadFinanciera::create([
                    'nombre' =>$request->nombre,
                    'entidad' =>$request->entidad,
                    'cuit' =>$request->cuit,
                    'estado'=>1

                ]);

            DB::commit();
            return response()->json($entidad);
        }
        catch (\Exception $e) {
            DB::rollback();
        }

    }


    /**
     * consigue entidad por id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getEntidadFinanciera($id)
    {
        $entidadFinanciera = EntidadFinanciera::find($id);

        if($entidadFinanciera) {
            return response()->json($entidadFinanciera);
        }

        return response()->json(['message' => 'Error al conseguir entidad !'], 404);
    }


    /**
     * consigue todos la lista de entidad financiera
     * @return \Illuminate\Http\JsonResponse
     */
    public function getFinancieras()
    {
        $entidadFinanciera = EntidadFinanciera::all();

        if($entidadFinanciera) {
            return response()->json($entidadFinanciera, 200);
        }else {
            return response()->json(['message' => 'Error al conseguir entidades!'], 404);
        }
    }

    /**
     * actualizar entidad
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public  function  updateEntidadFinanciera($id,Request $request)
    {

        try{
            DB::beginTransaction();

            $entidadFinanciera = EntidadFinanciera::find($id);

            if($request->nombre != null)
            {
                $entidadFinanciera->nombre = $request->nombre ;
            }

            if($request->entidad != null)
            {
                $entidadFinanciera->nombre = $request->entidad ;
            }

            if($request->cuit != null)
            {
                $entidadFinanciera->cuit = $request->cuit ;
            }

            if($request->estado != null)
            {
                $entidadFinanciera->estado = $request->estado ;
            }
            $entidadFinanciera->save();



            DB::commit();

            return response()->json($entidadFinanciera);
        }
        catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Error al actualizar!'], 404);
        }
    }

    /**
     * elimina registro de entidad
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function  deleteEntidadFinanciera($id)
    {
        try{
            DB::beginTransaction();

                EntidadFinanciera::destroy($id);

            DB::commit();

            return response()->json(['message' => 'Elemento eliminado!'], 200);
        }
        catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Error al actualizar!'], 404);
        }
    }
}
