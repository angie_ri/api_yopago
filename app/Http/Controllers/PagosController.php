<?php

namespace App\Http\Controllers;

use App\Cuenta;
use App\Pago;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PagosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');

    }

    /**
     * consigue datos de cuenta por nroCuenta
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function  getCuentaAPagar(Request $request)
    {
        $cuenta = Cuenta::where('nro_cuenta',$request->cuenta)
            ->where('estado',1)->first(['id_entidad','id_user']);

        if($cuenta != null)
        {
            $cuenta->user;
            return response()->json([
            'cuenta_data' => $cuenta,

            ]);
        }

        return response()->json(['message' => 'Error al conseguir entidad !'], 404);

    }

    /**
     * consigue datos de cuenta por nro-telefono
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function  getCuentaTelefonoAPagar(Request $request)
    {

        $user = User::where('telefono',$request->telefono)->first(['telefono','name','id']);

        if($user != null)
        {
            $user->cuenta();
            return response()->json([
                'telefono_data' => $user,
                'cuenta'=>$user->cuenta(),

            ]);
        }
        return response()->json(['message' => 'Error al conseguir entidad !'], 404);

    }


    /**
     * pagar compra/deuda por nrocuenta
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public  function pagarMontoPorCuenta(Request $request)
    {
        $cuenta = Cuenta::where('nro_cuenta',$request->cuenta)
            ->where('estado',1)->first(['id']);



        if($cuenta) {
            $validator =$request->validate([
                'monto' => 'required',
//            'id_cuenta' => 'required',
            ]);
            try {

                DB::beginTransaction();
                $pago = Pago::create([
                    'monto' => $request->monto,
                    'id_cuenta' => $cuenta->id,
                    'id_user' => Auth::id(),
                    'estado' => 1,
                ]);
                DB::commit();
                return response()->json($pago);

            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['message' => 'Error al pagar!', 'error' => $validator->errors(),$e], 404);
            }
        }else{
            return response()->json(['message' => 'No se encontro cuenta!'], 404);

        }
    }

    /**
     * pagar compra/deuda por cuenta-telefono
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function pagarMontoPorTelefono(Request $request)
    {
        $user = User::where('telefono',$request->telefono)->first(['telefono','id']);

        if($user) {
            $validator =$request->validate([
                'monto' => 'required',
//            'id_cuenta' => 'required',
            ]);

            try {
                DB::beginTransaction();
                $pago = Pago::create([
                    'monto' => $request->monto,
                    'id_cuenta' => $user->cuenta()->id,
                    'id_user' => Auth::id(),
                    'estado' => 1,
                ]);
                DB::commit();
                return response()->json($pago);

            } catch (\Exception $e) {
                DB::rollback();
                return response()->json(['message' => 'Error al pagar!', 'error' => $validator->errors(),$e], 404);
            }
        }else{
            return response()->json(['message' => 'No se encontro telefono!'], 404);

        }
    }

    /**
     * pagos realizados
     * @return \Illuminate\Http\JsonResponse
     */
    public function  getAllPagosRealizados(Request $request)
    {
        $pagos = Pago::where('id_user',$request->id_user)
            ->where('estado',1)->get();

        if($pagos){
            return response()->json($pagos);
        }else{
            return response()->json(['message'=>'No se encontraron pagos'],404);
        }
    }


    public function  getPagoRealizado($id)
    {
        $pago = Pago::where('id',$id)
            ->where('estado',1)->get();

        if($pago){
            return response()->json($pago);
        }else{
            return response()->json(['message'=>'No se encontraron pagos'],404);
        }
    }

    public  function  allPagos()
    {
        $pagos = Pago::where('id_user',Auth::id())->get();

        if($pagos){
            return response()->json($pagos);
        }else{
            return response()->json(['message'=>'No se encontraron pagos'],404);
        }
    }

    public function  getAllAPagar()
    {
        $pagos = Pago::where('id_user',Auth::id())
            ->where('estado',2)->get();

        if($pagos){
            return response()->json($pagos);
        }else{
            return response()->json(['message'=>'No se encontraron pagos'],404);
        }
    }

}
