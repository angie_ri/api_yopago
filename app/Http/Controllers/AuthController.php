<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\User;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    //

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required|min:6',
            'telefono'=>'required|unique',
        ]);

        $codigo=substr(rand(1, 99999), 0, 4);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => hash::make($request->password),
            'telefono'=>'54'.$request->telefono,
            'codigo'=>$codigo,

        ]);

        $basic  = new \Nexmo\Client\Credentials\Basic('12a603ca', 'q7xFypBfGXsGtg8l');
        $client = new \Nexmo\Client($basic);
//        '541122523914'
        $message = $client->message()->send([
            'to' => '54'.$request->telefono,
            'from' => 'Vonage SMS API',
            'text' =>'El codigo es '.$codigo,
        ]);

        return response()->json($user);
    }

    protected function generateAccessToken($user)
    {
        $token = $token = $user->createToken('micartera')->accessToken;
//        $user->createToken($user->email.'-'.now());

        return $token;
    }


    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email|exists:users,email',
            'password' => 'required',
            'codigo'=>'required'
        ]);
        $password = Hash::check('plain-text', $request->password);
        if( Auth::attempt(['email'=>$request->email, 'password'=>$request->password,'codigo'=>$request->codigo]) ) {
            $user = Auth::user();

            $token = $token = $user->createToken('micartera')->accessToken;

            return response()->json([
                'token' => $token,
                'message' => "Bienvenido/a"
            ]);
        }else{
            return response()->json([
                'message' => "Error de autentificación"
            ]);
        }


    }

    public function  logout(){

        $user = Auth::user();
        $user->tokens()->delete();
        $user->save();
        return response()->json([
            'message' => "Hasta pronto!"
        ]);

    }


}

