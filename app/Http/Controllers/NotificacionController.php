<?php

namespace App\Http\Controllers;

use App\Notificacion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotificacionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    private function  iniciarNexmoSms()
    {

        $basic  = new \Nexmo\Client\Credentials\Basic('12a603ca', 'q7xFypBfGXsGtg8l');
        $client = new \Nexmo\Client($basic);

        return $client;
    }

    /**
     * envia notificacion sms por pago realizado
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public  function  enviarNotificacionPago(Request $request)
    {
        $cliente= $this->$this->iniciarNexmoSms();

        try {

            DB::beginTransaction();
            $message = $cliente->message()->send([
                'to' => '54'.$request->telefono,
                'from' => 'Vonage SMS API',
                'text' =>'Se ha realizado el pago',
            ]);
            $notificacion = Notificacion::create([
                'id_user'=>$request->id_user,
                'telefono'=>$request->telefono,
                'tipo'=>1
            ]);
            DB::commit();
            //tipo 2 = cobro,1= pago
            if($notificacion){

                return response()->json(['message','Pago realizado'],200);
            }else{
                return response()->json(['message','No se ha podido realizar el pago'],404);

            }
        } catch (\Exception $e) {

            return response()->json(['message' => 'Error de notificación!'], 404);

            DB::rollback();
        }
    }


    /**
     * notifica si se por cobro
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public  function  enviarNotificacionCobro(Request $request)
    {
        $cliente= $this->$this->iniciarNexmoSms();
        try {
            DB::beginTransaction();
            $message = $cliente->message()->send([
                'to' => '54'.$request->telefono,
                'from' => $request->name,
                'text' =>'Tienes un pago pendiente',
            ]);

            $notificacion = Notificacion::create([
                'id_user'=>$request->id_user,
                'telefono'=>$request->telefono,
                'tipo'=>2
            ]);
            DB::commit();
            if($notificacion){
                return response()->json(['message','Cobro realizado'],200);
            }else{
                return response()->json(['message','No se ha podido comunicar el cobro'],404);

            }
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Error de notificación!'], 404);


        }
    }


    /**
     * consigue notificaciones de pagos
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNotificacionesPagos($id_user)
    {
        $notificaciones = Notificacion::where('tipo',1)
            ->where('id_user',$id_user)->get();

        if($notificaciones){
            return response()->json($notificaciones,200);
        }else{
            return response()->json(['message'=>'No se encontraron notificaciones'],200);
        }
    }


    /**
     * consigue notificaciones de cobro
     * @param $id_user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNotificacionesCobros($id_user)
    {
        $notificaciones = Notificacion::where('tipo',2)
            ->where('id_user',$id_user)->get();

        if($notificaciones){
            return response()->json($notificaciones,200);
        }else{
            return response()->json(['message'=>'No se encontraron notificaciones'],200);
        }
    }

    /**
     * consigue todas las notificaciones de usuario
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public  function  getNotificaciones($id_user){

        $notificacion = Notificacion::where('id_user',$id_user)->get();

        if($notificacion){
            return response()->json($notificacion,200);
        }else{
            return response()->json(['message'=>'No se encontraron notificaciones'],200);
        }
    }
}
