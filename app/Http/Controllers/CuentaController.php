<?php


namespace App\Http\Controllers;


use App\Cuenta;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CuentaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');

    }

    /**
     * ingresar nueva cuenta
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function registrarCuenta(Request $request)
    {
        $validator =$request->validate([
            'id_entidad' => 'required',
            'nro_cuenta' => 'required|unique:cuentas',
        ]);

        try {
            DB::beginTransaction();

            $user = Auth::id();

            $cuenta = Cuenta::create([
                'id_entidad' => $request->id_entidad,
                'nro_cuenta' => $request->nro_cuenta,
                'id_user' => $user,
                'estado' => 1,
            ]);

            DB::commit();
            return response()->json($cuenta);

        } catch (\Exception $e) {

            return response()->json(['message' => 'Error al crear cuenta!','error'=>$validator->errors()], 404);

            DB::rollback();
        }
    }

    /**
     * consigue cuenta por id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function  getCuenta($id)
    {
        $cuenta = Cuenta::find($id);

        if($cuenta) {
            return response()->json($cuenta);
        }

        return response()->json(['message' => 'Error al conseguir entidad !'], 404);
    }


    /**
     * consigue todas las cuentas
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCuentas()
    {
        $cuentas = Cuenta::all();

        if($cuentas) {
            return response()->json($cuentas, 200);
        }else {
            return response()->json(['message' => 'Error al conseguir entidades!'], 404);
        }
    }


    /**
     * actualiza cuenta por id
     * @param $id
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public  function  updateCuenta($id,Request $request)
    {

        try{
            DB::beginTransaction();

            $cuenta = Cuenta::find($id);

            if($request->id_entidad != null)
            {
                $cuenta->id_entidad = $request->id_entidad ;
            }

            if($request->nro_cuenta != null)
            {
                $cuenta->nro_cuenta = $request->nro_cuenta ;
            }

            if($request->estado != null)
            {
                $cuenta->estado = $request->estado ;
            }
            $cuenta->save();

            DB::commit();

            return response()->json($cuenta);
        }
        catch (\Exception $e) {
            DB::rollback();
            return response()->json(['message' => 'Error al actualizar!'], 404);
        }
    }

    /**
     * elimina cuenta por id
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function  deleteCuenta($id)
    {
        try{
            DB::beginTransaction();

                Cuenta::destroy($id);

            DB::commit();

            return response()->json(['message' => 'Elemento eliminado!'], 200);
        }
        catch (\Exception $e) {
            DB::rollback();
                return response()->json(['message' => 'Error al actualizar!'], 404);
        }
    }

}

