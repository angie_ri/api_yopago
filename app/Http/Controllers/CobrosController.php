<?php

namespace App\Http\Controllers;

use App\Cobro;
use App\Cuenta;
use App\Pago;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class CobrosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');

    }

    /**
     * consulta cuenta por cobrar por nro cuenta
     * @param $nro_cuenta
     * @return \Illuminate\Http\JsonResponse
     */
    public function  getCuentaACobrar($nro_cuenta)
    {
        $cuenta = Cuenta::where('nro_cuenta',$nro_cuenta)
            ->where('estado',1)->first(['id_entidad','id_user']);
        if($cuenta != null)
        {
             $cuenta->user;
            return response()->json([
                'cuenta_data' => $cuenta,

            ]);
        }

        return response()->json(['message' => 'Error al conseguir entidad !'], 404);

    }

    /**
     * consigue datos de cuenta a cobrar por nro-telefono
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function  getCuentaTelefonoACobrar($telefono)
    {

        $user = User::where('telefono',$telefono)->first(['telefono','name','id']);


        if($user != null)
        {
            $user->cuenta();
            return response()->json([
                'telefono_data' => $user,
                'cuenta'=>$user->cuenta(),

            ]);
        }
        return response()->json(['message' => 'Error al conseguir entidad !'], 404);

    }


    /**
     *Registra el cobro por nro cuenta
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public  function cobrarPorCuenta(Request $request)
    {
        $cuenta = Cuenta::where('nro_cuenta',$request->cuenta)
            ->where('estado',1)->first(['id']);

        try {

            $validator =$request->validate([
                'monto' => 'required',

            ]);
            if($cuenta) {
                DB::beginTransaction();
                $cobro = Cobro::create([
                    'monto' => $request->monto,
                    'id_cuenta' => $cuenta->id,
                    'id_user' => Auth::id(),
                    'estado' => 1,
                ]);

                DB::commit();
                return response()->json($cobro);
            }else{
                return response()->json(['message' => 'Error al cobrar!, debe ingresa una cuenta y el monto a cobrar', 'error' => $validator->errors()], 404);
            }

        } catch (\Exception $e) {

            DB::rollback();
            return response()->json(['message' => 'Error al cobrar!', 'error' => $e], 404);

        }
    }

    /**
     * registra cobro por cuenta-telefono
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function cobrarPorTelefono(Request $request)
    {
        $user = User::where('telefono',$request->telefono)->first(['telefono','id']);

        if($user){
            $validator =$request->validate([
                'monto' => 'required',
//            'id_cuenta' => 'required',
            ]);
            try {

                DB::beginTransaction();
                $cobro = Cobro::create([
                    'monto' => $request->monto,
                    'id_cuenta' => $user->cuenta()->id,
                    'id_user' => Auth::id(),
                    'estado' => 1,
                ]);
                DB::commit();
                return response()->json($cobro);

            } catch (\Throwable $e) {
                DB::rollback();
                return response()->json(['message' => 'Error al cobrar!', 'error' => $validator->errors(),$e], 404);
            }
        }else {
            return response()->json(['message' => 'Error al cobrar!, debe ingresar un nro de telefono y el monto a cobrar'], 404);
        }
    }


    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function  getAllCobrosPendientes()
    {
        $cobros = Cobro::where('id_user',Auth::id())
            ->where('estado',1)->get();

        if($cobros){
            return response()->json($cobros);
        }else{
            return response()->json(['message'=>'No se encontraron pagos'],404);
        }
    }


    public function  getCobroPendiente($id)
    {
        $cobro = Cobro::where('id',$id)
            ->where('estado',1)->get();

        if($cobro){
            return response()->json($cobro);
        }else{
            return response()->json(['message'=>'No se encontraron pagos'],404);
        }
    }

    public  function  allCobros($id_user)
    {
        $cobros=Cobro::where('id_user',$id_user)->get();

        if($cobros){
            return response()->json($cobros);
        }else{
            return response()->json(['message'=>'No se encontraron pagos'],404);
        }
    }

    public function  getAllCobrosRealizados($id_user)
    {
        $cobros = Cobro::where('id_user',$id_user)
            ->where('estado',2)->get();

        if($cobros){
            return response()->json($cobros);
        }else{
            return response()->json(['message'=>'No se encontraron pagos'],404);
        }
    }
}
