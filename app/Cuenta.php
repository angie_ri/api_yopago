<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuenta extends Model
{
    protected $table='cuentas';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'id_entidad', 'nro_cuenta','estado','id_user',
    ];

    public function user()
    {
        return $this->hasOne('App\User','id','id_user');
    }

    public  function userCuenta()
    {
        return $this->morphTo();
    }



}
