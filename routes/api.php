<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('/registrar', 'AuthController@register');
Route::post('/ingresar', 'AuthController@login');
//Route::get('/entidadFinaciera/lista','EntidadFinancieraController@allEntidadFinanciera');

Route::middleware('auth:api')->group(function() {

    Route::post('/logout', 'AuthController@logout');
    Route::get('user/{id}/detalles', 'UserController@show');

    //entidades
    Route::post('/entidadFinaciera/crear','EntidadFinancieraController@registrasEntidadFinanciera');
    Route::get('/entidadFinaciera/{id}','EntidadFinancieraController@getEntidadFinanciera');
    Route::post('/entidadFinaciera/entidades','EntidadFinancieraController@getFinancieras');
    Route::post('/entidadFinaciera/actualizar/{id}','EntidadFinancieraController@updateEntidadFinanciera');
    Route::get('/entidadFinaciera/eliminar/{id}','EntidadFinancieraController@deleteEntidadFinanciera');

    //cuentas
    Route::post('/cuenta/crear','CuentaController@registrarCuenta');
    Route::get('/cuenta/{id}','CuentaController@getCuenta');
    Route::post('/cuenta/lista','CuentaController@getCuentas');
    Route::post('/cuenta/actualizar/{id}','CuentaController@updateCuenta');
    Route::get('/cuenta/eliminar/{id}','CuentaController@deleteCuenta');

    //pagos
    Route::post('/pago/cuenta','PagosController@getCuentaAPagar');
    Route::post('/pago/telefono','PagosController@getCuentaTelefonoAPagar');
    Route::post('/pago/pagar/cuenta','PagosController@pagarMontoPorCuenta');
    Route::post('/pago/pagar/telefono','PagosController@pagarMontoPorTelefono');
    Route::post('/pago/lista','PagosController@getAllPagosRealizados');
    Route::get('/pago/{id}','PagosController@getPagoRealizado');
    Route::post('/pago/all','PagosController@allPagos');
    Route::post('/pago/pendientes','PagosController@getAllAPagar');


    //cobros otra forma de consultar
    Route::get('/cobro/cuenta/{cuenta}','CobrosController@getCuentaACobrar');
    Route::get('/cobro/telefono/{telefono}','CobrosController@getCuentaTelefonoACobrar');
    Route::post('/cobro/cobrar/cuenta','CobrosController@cobrarPorCuenta');
    Route::post('/cobro/cobrar/telefono','CobrosController@cobrarPorTelefono');
    Route::post('/cobro/lista','CobrosController@getAllCobrosPendientes');
    Route::get('/cobro/pendiente/{id}','CobrosController@getCobroPendiente');
    Route::get('/cobro/all/{id_user}','CobrosController@allCobros');
    Route::get('/cobro/cobrados/{id_user}','CobrosController@getAllCobrosRealizados');

    //caja
    Route::get('/caja/{id_user}','CajaController@getCaja');
    Route::post('/caja/crear','CajaController@newCaja');
    Route::get('/caja/saldo/{id}','CajaController@getSaldo');
    Route::get('/caja/deuda/{id}','CajaController@getDeuda');
    Route::get('/caja/deuda/{id}','CajaController@getDeuda');
    Route::post('/caja/deuda','CajaController@setDeuda');
    Route::post('/caja/saldo','CajaController@setSaldo');
    Route::post('/caja/cerrar','CajaController@cerrarCaja');

    //notificaciones

    Route::post('/notificacion/pago','NotificacionController@enviarNotificacionPago');
    Route::post('/notificacion/cobro','NotificacionController@enviarNotificacionCobro');
    Route::get('/notificacion/pagos/{id_user}','NotificacionController@getNotificacionesPagos');
    Route::get('/notificacion/cobros/{id_user}','NotificacionController@getNotificacionesCobros');
    Route::get('/notificacion/all/{id_user}','NotificacionController@getNotificaciones');
});
